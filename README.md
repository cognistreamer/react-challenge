![EY CogniStreamer logo](./images/logo.png)

# React Coding Challenge

We are happy to have you participating in this challenge as part of the recruitment process for the
front-end developer position within our team! Good luck!

## Introduction

This document describes a coding challenge that tackles various core-concepts used in day-to-day
(React) development, such as:

-   Data-fetching (using the public GitHub REST API or GraphQL API if you prefer)
-   State management (classes or hooks)
-   Component lifecycles (classes or hooks)
-   Identification and sharing of common (component) logic
-   Managing various UI states within components (empty, loading, error, ...)
-   Some basic styling (css, css-modules or css-in-js flavour of the month)
-   Various React and JavaScript basics (error handling, promises, event handlers, JSX, data
    parsing, props, …)

Other things have been deliberately left out to keep the scope of this challenge small enough. This
includes topics like routing, testing, forms, accessibility, complex state management (i.e. redux),
and so on. Feel free to completely disregard these.

## The app

The app we're going to make is a pretty basic GitHub profile viewer. You have a search box, you
enter a GitHub username and press enter, the app fetches the corresponding profile data. When
watching a profile you can view a list of repositories, followers and users that are followed, and
that’s that!

In total there are 4 API endpoints you have to use:

-   _Fetching the profile information_: `https://api.github.com/users/{userName}`
-   _Fetching the repositories_: `https://api.github.com/users/{userName}/repos`
-   _Fetching the followers_: `https://api.github.com/users/{userName}/followers`
-   _Fetching the users followed_: `https://api.github.com/users/{userName}/following`

**The GitHub REST API (v3) documentation can be found [here](https://developer.github.com/v3).**

Of course, there’s some things to keep in mind while implementing this:

-   The profile the user is searching for might not exist
-   The API request might fail or the connection could drop
-   The app needs to support multiple subsequent searches for different profiles
-   The app needs to be usable on a smaller screen

### And now… screenshots!

The main view of the app can be seen in the image below. This is a reference layout, we don’t expect
your version to be exactly the same.

![App view](./images/app_view.png)

When a user browses to the app they should only see an empty search box with a submit button.

![Initial state](./images/initial_state.png)

While a profile is being loaded there should be some sort of loading indicator, for example
something similar to the one below.

![Loading a profile](./images/loading_profile.png)

If an error occurs it needs to be communicated to the user. There's no need to go into details about
what happened, or the type of error.

![Error while loading a profile](./images/error_loading_profile.png)

If the profile data was successfully loaded, the app should jump immediately to the first tab
(Repositories) and also load the corresponding repository data. When loading a subsequent profile,
the first tab needs to be selected again.

**The data of the two other tabs (followers & following) should only be loaded after the user
actually clicks on the corresponding tab.**

When clicking on a user from the “Followers” or “Following” list, the currently loaded profile
should be replaced by the profile of the user that was selected. It is important that the search box
also gets updated and reflects this profile.

And that’s about it! All of this might be a little bit clearer with a video (well, gif…):

![Quick overview](./images/overview.gif)

## Goals of this exercise

The purpose of this coding challenge is to get an understanding of your experience and skill-level.
**We do not expect you to create a pixel-by-pixel reproduction of the reference app** because it is
just that - a reference. Take your time, do your thing, have a look at the requirements and see if
you can get everything hooked up and working without stressing too much over the details. We're
mostly interested in your approach, decisions and motivations, and of course the quality of your
code.

## Setup & implementation

A reference implementation of this app has been implemented with
[create-react-app](https://facebook.github.io/create-react-app). The only libraries we have used are
`react`, `react-dom` and `prop-types`, however, you are free to use any library you want (as long as
it includes `react`). The reference implementation uses the
[Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch) to query the
GitHub REST API and also heavily uses
[function components & react hooks](https://reactjs.org/docs/hooks-state.html#hooks-and-function-components).
This last part is not a requirement seeing as this is a fairly recent development within the React
ecosystem, but it's definitely a nice-to-have. If you prefer to use something like TypeScript or
ReasonReact, feel free to. We want to learn!

Some things we would really love to see:

-   Clear separation of concerns (layout components, data management & state components, common
    utilities, ...)
-   Clean & readable code
-   Composing, sharing or reusing of components wherever it makes sense (but don’t over-abstract!)
-   Thorough understanding and correct usage of React-specific concepts
-   Some sensible styling conventions (i.e. BEM)
    -   If css-modules or css-in-js libraries are your jam, go ahead!
-   Bonus points: usage of
    [React hooks](https://reactjs.org/docs/hooks-state.html#hooks-and-function-components)
-   Additional bonus points: elimination of class components by using function components (while
    still optimizing where possible through i.e. the use of memoization)

### Getting up and running quickly

To get started quickly you can clone our
[react-challenge-template](https://bitbucket.org/cognistreamer/react-challenge-template) project on
BitBucket, which is an app that has been bootstrapped with
[create-react-app](https://facebook.github.io/create-react-app) and should be ready to go. See the
`README.md` file inside of the template project for instructions on how to start the local
development server. You’re free to do as you wish, however, so if you want to spin up your own
project, go ahead!

## Delivery & next steps

The best way to get the code back to us is to create a GitHub or Bitbucket repository with your
(forked) project and send us back a link. If you would prefer to keep your code private you can add
one of our developers as a collaborator to your project by adding the user "pleunv" (can be found on
both GitHub & Bitbucket). In case this does not work for you, please get back to us via email and
we'll work out another solution.

A follow-up interview might be scheduled if we need additional clarification of some parts of the
implementation, or if we would like to go through the code together with you. Don’t panic, this does
not mean it is bad, it just means we are curious. Or super excited to talk to you again!

Please include the following people in your communication:

-   **Amélie De Troz** (_Senior Recruiter IT & Technology_,
    [amelie.de.troz@be.ey.com](mailto:amelie.de.troz@be.ey.com))
-   **Pleun Vanderbauwhede** (_Front-end Developer_,
    [pleun.vanderbauwhede@be.ey.com](pleun.vanderbauwhede@be.ey.com))
-   **Frank Naessens** (_Product Manager_,
    [frank.naessens@be.ey.com](pleun.vanderbauwhede@be.ey.com))

We hope all of this was clear enough to get you started! If there are any problems or something
needs additional clarification, please do not hesitate to get in touch with us.
